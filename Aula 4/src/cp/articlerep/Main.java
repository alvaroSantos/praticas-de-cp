package cp.articlerep;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
	// BEGIN of data for sanity check
	public static final boolean SANITY_CHECK = true;
	public static final java.util.Set<String> addedStrings = new java.util.concurrent.ConcurrentSkipListSet<String>();
	// END of data for sanity check

	private static String[] wordArray;

	private static void populateWordArray(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line;
		int i = 0;

		String firstLine = br.readLine();
		int dictSize = Integer.parseInt(firstLine);
		wordArray = new String[dictSize];

		while ((line = br.readLine()) != null && i < dictSize) {
			wordArray[i] = line;
			i++;
		}

		if (i < dictSize) {
			dictSize = i;
		}

		br.close();
	}

	public static void main(String[] args) throws InterruptedException, IOException {

		if (args.length < 2) {
			System.out.println("Usage: " + Main.class.getCanonicalName() + " time_in_milisecs num_threads");
			System.exit(0);
		}

		// get command line arguments
		Integer time = Integer.parseInt(args[0]);
		Integer numThreads = Integer.parseInt(args[1]);

		StopVar stopVar = new StopVar();
		Worker[] workers = new Worker[numThreads];

		System.out.println("Starting application...");

		populateWordArray("resources/dictionary.txt");

		// wait for the specified time
		for(int k = 0; k < 3; ++k) {
			// launch the threads
			for (int i = 0; i < numThreads; i++) {
				workers[i] = new Worker(wordArray, stopVar);
				workers[i].start();
			}

			Thread.sleep(time);
			stopVar.stop = true;

			// claculate statistics
			int sumOps = 0;
			int sumInsertOps = 0;
			int sumRemoveOps = 0;
			int sumContainsOps = 0;

			for (int i = 0; i < numThreads; i++) {
				workers[i].join();
				sumOps += workers[i].getNumOps();
				sumInsertOps += workers[i].getSuccInsertOps();
				sumRemoveOps += workers[i].getSuccRemoveOps();
				sumContainsOps += workers[i].getSuccContainsOps();
			}

			for(int i = 0; i < numThreads; ++i) {
				if(!workers[i].validate()) {
					System.out.println("CATASTROPHIC FAILURE ABORT NOW !!!!!!!!!!!!!!!");
				}
			}

			// do sanity sheck
			if (SANITY_CHECK) {
				for (String str : addedStrings) {
					if (!Worker.getSharedSet().contains(str)) {
						throw new RuntimeException("String " + str + " does not exist in set!");
					}
				}
			}

			// print statistics
			System.out.println("Total operations: " + sumOps);
			System.out.println("Total successful inserts: " + sumInsertOps + " ("
					+ Math.round(sumInsertOps / (double) sumOps * 100.0) + "%)");
			System.out.println("Total successful removes: " + sumRemoveOps + " ("
					+ Math.round(sumRemoveOps / (double) sumOps * 100.0) + "%)");
			System.out.println("Total successful contains: " + sumContainsOps + " ("
					+ Math.round(sumContainsOps / (double) sumOps * 100.0) + "%)");
			System.out.println("Throughput: " + (Math.round(sumOps / (time / 1000.0))) + " ops/sec");

			stopVar.stop = false;
		}

		System.out.println("Finished application.");

	}
}
