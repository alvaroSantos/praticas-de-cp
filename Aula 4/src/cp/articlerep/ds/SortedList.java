package cp.articlerep.ds;

public class SortedList<T extends Comparable<T>> implements Set<T> {
	class Node<E> {
		private final E value;
		private Node<E> next;

		public Node(E value, Node<E> next) {
			this.value = value;
			this.next = next;
		}

		public E getValue() {
			return value;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}
	}

	private Node<T> head;

	public SortedList() {
		head = null;
	}

	public boolean insert(T valueToInsert) {
		if (head == null) {
			head = new Node<T>(valueToInsert, null);
			++insertions;
			return true;
		} else if (head.getValue().compareTo(valueToInsert) == 0) {
			return false;
		} else if (head.getValue().compareTo(valueToInsert) > 0) {
			Node<T> nodeToInsert = new Node<T>(valueToInsert, head);
			head = nodeToInsert;
			++insertions;
			return true;
		} else {
			Node<T> previousNode = null;
			Node<T> currentNode = head;
			while (currentNode != null && currentNode.getValue().compareTo(valueToInsert) < 0) {
				previousNode = currentNode;
				currentNode = currentNode.getNext();
			}
			if (currentNode == null || currentNode.getValue().compareTo(valueToInsert) > 0) {
				// value is not in list, insert between previousNode and currentNode
				Node<T> nodeToInsert = new Node<T>(valueToInsert, currentNode);
				previousNode.setNext(nodeToInsert);
				++insertions;
				return true;
			} else {
				// currentNode has value
				return false;
			}
		}
	}

	public boolean remove(T valueToRemove) {
		if (head == null) {
			return false;
		} else if (head.getValue().compareTo(valueToRemove) == 0) {
			head = head.getNext();
			++removals;
			return true;
		} else {
			Node<T> previousNode = head;
			Node<T> currentNode = head.getNext();
			while (currentNode != null && currentNode.getValue().compareTo(valueToRemove) < 0) {
				previousNode = currentNode;
				currentNode = currentNode.getNext();
			}
			if (currentNode == null || currentNode.getValue().compareTo(valueToRemove) > 0) {
				// value is not in list
				return false;
			} else {
				// currentNode has value, remove it
				previousNode.setNext(currentNode.getNext());
				++removals;
				return true;
			}
		}
	}

	public boolean contains(T value) {
		Node<T> currentNode = head;
		while (currentNode != null && currentNode.getValue().compareTo(value) < 0) {
			currentNode = currentNode.getNext();
		}
		return currentNode != null && currentNode.getValue().compareTo(value) == 0;
	}

	int insertions, removals;

	public boolean validate() {
		int elements = 0;

		Node<T> h = head;
		while (h != null) {
			++elements;
			Node<T> n = h.getNext();

			if (n != null) {
				int cmp = h.getValue().compareTo(n.getValue());

				if (cmp >= 0) {
					return false;
				}
			}

			h = n;
		}

		if (elements < 0) {
			return false;
		}

		if (insertions - removals != elements) {
			return false;
		}

		return true;
	}
}
