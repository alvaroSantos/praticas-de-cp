package cp.articlerep.ds;

public interface Set<T extends Comparable<T>> {
    public boolean insert(T value);
    public boolean remove(T value);
    public boolean contains(T value);
    public boolean validate();
}
