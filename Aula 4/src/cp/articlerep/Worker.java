package cp.articlerep;

import java.util.Random;

import cp.articlerep.ds.SortedList;
import cp.articlerep.ds.Set;

public class Worker extends Thread {
	private final static Set<String> sharedSet = new SortedList<String>();

	private final Random rand = new Random();
	private final String[] sharedDict;
	private final StopVar sharedStop;

	private int numOps = 0;
	private int succInsertOps = 0;
	private int succRemoveOps = 0;
	private int succContainsOps = 0;

	public Worker(String[] sharedDict, StopVar sharedStop) {
		super();
		this.sharedDict = sharedDict;
		this.sharedStop = sharedStop;
	}

	@Override
	public void run() {
		while (!sharedStop.stop) {
			int op = rand.nextInt(100);
			int id = rand.nextInt(sharedDict.length);
			String str = sharedDict[id];
			if (op < 25) {
				// probability 25% of trying to insert 'str'
				if (sharedSet.insert(str)) {
					succInsertOps++;
					if (Main.SANITY_CHECK) {
						Main.addedStrings.add(str);
					}
				}
			} else if (op < 50) {
				// probability 25% of trying to remove 'str'
				if (sharedSet.remove(str)) {
					succRemoveOps++;
					if (Main.SANITY_CHECK) {
						Main.addedStrings.remove(str);
					}
				}
			} else {
				// probability 50% of trying to lookup for 'str'
				if (sharedSet.contains(str)) {
					succContainsOps++;
				}
			}
			numOps++;
		}
	}

	public int getNumOps() {
		return numOps;
	}

	public int getSuccInsertOps() {
		return succInsertOps;
	}

	public int getSuccRemoveOps() {
		return succRemoveOps;
	}

	public int getSuccContainsOps() {
		return succContainsOps;
	}

	public static Set<String> getSharedSet() {
		return sharedSet;
	}

	public boolean validate() {
		return sharedSet.validate();
	}
}
